<!-- vale off -->
# Docker Terraform Inventory
<!-- vale on -->

[![Software License](https://img.shields.io/badge/license-MIT-informational.svg?style=flat)](LICENSE)
[![Pipeline Status](https://gitlab.com/op_so/docker/terraform-inventory/badges/main/pipeline.svg)](https://gitlab.com/op_so/docker/terraform-inventory/pipelines)

A [Terraform](https://www.terraform.io/) Docker image:

* **lightweight** image based on Alpine Linux only #50 MB,
* `multiarch` with support of **amd64** and **arm64**,
* **non-root** container user,
* **automatically** updated by comparing software bill of materials (`SBOM`) changes,
* image **signed** with [Cosign](https://github.com/sigstore/cosign),
* a **software bill of materials (`SBOM`) attestation** added using [`Syft`](https://github.com/anchore/syft),
* available on **Docker Hub** and **Quay.io**.

[![GitLab](https://shields.io/badge/Gitlab-informational?logo=gitlab&style=flat-square)](https://gitlab.com/op_so/docker/terraform-inventory) The main repository.

[![Docker Hub](https://shields.io/badge/dockerhub-informational?logo=docker&logoColor=white&style=flat-square)](https://hub.docker.com/r/jfxs/terraform-inventory) The Docker Hub registry.

[![Quay.io](https://shields.io/badge/quay.io-informational?logo=docker&logoColor=white&style=flat-square)](https://quay.io/repository/ifxs/terraform-inventory) The Quay.io registry.

`Terraform` has a fixed `1.5.*` version to keep the [Mozilla Public License (MPL)](https://www.mozilla.org/en-US/MPL/).
<!-- vale off -->
All other components are updated.
<!-- vale on -->
This image also includes a Python program [`state2inventory`](https://gitlab.com/op_so/ansible/state2inventory) to generate an `Ansible` inventory from an infrastructure state.

## Running terraform and state2inventory

```shell
docker run -t --rm jfxs/terraform-inventory terraform version
docker run -t --rm jfxs/terraform-inventory state2inventory terraform --help
```

or

```shell
docker run -t --rm quay.io/ifxs/terraform-inventory terraform version
docker run -t --rm quay.io/ifxs/terraform-inventory state2inventory terraform --help
```

## Built with

Docker latest tag is [--VERSION--](https://gitlab.com/op_so/docker/terraform-inventory/-/blob/main/Dockerfile) and has:

<!-- vale off -->
--SBOM-TABLE--
<!-- vale on -->

[`Dockerhub` Overview page](https://hub.docker.com/r/jfxs/terraform-inventory) has the details of the last published image.

## Versioning

Docker tag definition:

* the Terraform version used,
* a dash
* an increment to differentiate build with the same version starting at 001

```text
<terraform_version>-<increment>
```

<!-- vale off -->
Example: 1.5.7-003
<!-- vale on -->

## Signature and attestation

[Cosign](https://github.com/sigstore/cosign) public key:

```shell
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEa3yV6+yd/l4zh/tfT6Tx+zn0dhy3
BhFqSad1norLeKSCN2MILv4fZ9GA6ODOlJOw+7vzUvzZVr9IXnxEdjoWJw==
-----END PUBLIC KEY-----
```

The public key is also available online: <https://gitlab.com/op_so/docker/cosign-public-key/-/raw/main/cosign.pub>.

To verify an image:

```shell
cosign verify --key cosign.pub $IMAGE_URI
```

To verify and get the software bill of materials (`SBOM`) attestation:

```shell
cosign verify-attestation --key cosign.pub --type spdxjson $IMAGE_URI | jq '.payload | @base64d | fromjson | .predicate'
```

## Authors

<!-- vale off -->
* **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)
<!-- vale on -->

## License

<!-- vale off -->
This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
<!-- vale on -->
